

"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_shop = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        title: Joi.string().required().error(new Error('title is required!')),
        Subtitle: Joi.string().error(new Error('Subtitle is string!')),
        image: Joi.array().required().error(new Error('image is required!')),
        Address: Joi.string().required().error(new Error('address is required!')),
        state: Joi.string().required().error(new Error('state is required!')),
        city: Joi.string().required().error(new Error('city is required!')),
        Pincode: Joi.number().required().error(new Error('Pincode is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        Rating: Joi.number().required().error(new Error('Rating is required!')),
        orderType: Joi.number().error(new Error('orderType is number')),
        SubRating: Joi.number().required().error(new Error('SubRating is required!')),
        OpeningTime: Joi.string().required().error(new Error('OpeningTime is required!')),
        EndingTime: Joi.string().required().error(new Error('EndingTime is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
export const update_shop = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        id: Joi.string().required().error(new Error('id is required!')),
        title: Joi.string().required().error(new Error('title is required!')),
        Subtitle: Joi.string().error(new Error('Subtitle is string!')),
        image: Joi.array().required().error(new Error('image is required!')),
        Address: Joi.string().required().error(new Error('address is required!')),
        state: Joi.string().required().error(new Error('state is required!')),
        city: Joi.string().required().error(new Error('city is required!')),
        Pincode: Joi.number().required().error(new Error('Pincode is required!')),
        Price: Joi.number().required().error(new Error('Price is required!')),
        Rating: Joi.number().required().error(new Error('Rating is required!')),
        orderType: Joi.number().error(new Error('position is number')),
        SubRating: Joi.number().required().error(new Error('SubRating is required!')),
        OpeningTime: Joi.string().required().error(new Error('OpeningTime is required!')),
        EndingTime: Joi.string().required().error(new Error('EndingTime is required!')),
        location: Joi.object().required().error(new Error('location is required!')),
    })
    schema.validateAsync(req.body).then(result => {
        if (!isValidObjectId(result.id)) return res.status(400).json(new apiResponse(400, "invalid id format", {}, {}));
        return next()
    }).catch(error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}



