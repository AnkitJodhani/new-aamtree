"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_product = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        title: Joi.string().max(50).required().error(new Error('title is required! & max length is 50')),
        image: Joi.array().required().error(new Error('image is must be a array')),
        price: Joi.number().required().error(new Error('Price is required!')),
        Quantity: Joi.number().required().error(new Error('Quantity is required!')),
        categoryId: Joi.string().required().trim().error(new Error('companyId is required!')),

    })
    schema.validateAsync(req.body).then(async result => {
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}
export const update_product = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        id: Joi.string().required().error(new Error('id is required!')),
        title: Joi.string().max(50).required().error(new Error('title is required! & max length is 50')),
        image: Joi.array().required().error(new Error('image is must be a array')),
        price: Joi.number().required().error(new Error('Price is required!')),
        Quantity: Joi.number().required().error(new Error('Quantity is required!')),
        categoryId: Joi.string().required().trim().error(new Error('companyId is required!')),
    })
    schema.validateAsync(req.body).then(async result => {
        if (!isValidObjectId(result.id)) return res.status(400).json(new apiResponse(400, "Invalid id format", {}, {}));
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}

