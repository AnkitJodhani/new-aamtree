"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const add_category = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        name: Joi.string().max(50).required().error(new Error('name is required! & max length is 50')),
        image: Joi.string().error(new Error('image is must be a string')),
    })
    schema.validateAsync(req.body).then(async result => {
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}

export const update_category = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        id: Joi.string().required().error(new Error('id is required!')),
        name: Joi.string().max(50).error(new Error('name is required! & max length is 50')),
        image: Joi.string().error(new Error('image is required!')),
    })
    schema.validateAsync(req.body).then(async result => {
        if (!isValidObjectId(result.id)) return res.status(400).json(new apiResponse(400, "Invalid id format", {}, {}));
        return next()
    }).catch(async error => { res.status(400).json(new apiResponse(400, error.message, {}, {})) })
}

