"use strict"
import * as Joi from "joi"
import { apiResponse } from '../common'
import { isValidObjectId } from 'mongoose'
import { Request, Response } from 'express'

export const signUp = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        phoneNumber: Joi.string().required().error(new Error('phoneNumber is required!')),
        countryCode: Joi.number().required().error(new Error('countryCode is required!')),
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => {
        res.status(400).json(new apiResponse(400, error.message, {}, {}))
    })
}
export const login = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        phoneNumber: Joi.number().required().error(new Error('phoneNumber is required!')),
        countryCode: Joi.number().required().error(new Error('countryCode is required!')),
        deviceToken: Joi.string().error(new Error('deviceToken is string!')),
    })
    schema.validateAsync(req.body).then(async result => {
        req.body = result
        return next()
    }).catch(async error => {
        res.status(400).json(new apiResponse(400, error.message, {}, {}));
    })
}

export const update_profile = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        firstName: Joi.string().error(new Error('firstName is string')),
        lastName: Joi.string().error(new Error('lastName is string')),
        phoneNumber: Joi.string().error(new Error('phoneNumber is string')),
        email: Joi.string().allow("").error(new Error('email is string! ')),
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => {
        res.status(400).json(new apiResponse(400, error.message, {}, {}))
    })
}
export const user_otp_verification = async (req: Request, res: Response, next: any) => {
    const schema = Joi.object({
        otp: Joi.number().min(100000).max(999999).required().error(new Error('otp is required! & only is 6 digits'))
    })
    schema.validateAsync(req.body).then(result => {
        return next()
    }).catch(error => {
        res.status(400).json(new apiResponse(400, error.message, {}, {}))
    })
}   