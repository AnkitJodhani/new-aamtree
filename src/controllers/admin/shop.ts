"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { shopModel } from '../../database'
import { apiResponse, not_first_one, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'

const ObjectId = mongoose.Types.ObjectId


export const add_shop = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    let user: any = req.header('user')
    try {
        let isExist = await shopModel.findOne({ title: req.body.title, isActive: true })
        if (isExist) return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('shop'), {}, {}));
        req.body.createdBy = (req.header('user') as any)?._id
        let response = await new shopModel(req.body).save()
        if (response) return res.status(409).json(new apiResponse(409, responseMessage?.addDataSuccess('shop'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));


    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, "Internal Server Error", {}, error))
    }
}
export const get_shop = async (req: Request, res: Response) => {
    reqInfo(req)
    let { state, city, page, limit } = req.body, match: any = {}, shop_data: any, shop_count: any
    try {
        match.isActive = true
        if (state)
            match.state = state
        if (city)
            match.city = city
        const [shop_data, shop_count] = await Promise.all([
            await shopModel.aggregate([
                { $match: match },
                { $sort: { createdAt: -1 } },
                { $skip: ((((page) as number - 1) * (limit) as number)) },
                { $limit: (limit) as number },
                {
                    $project: {
                        _id: 1,
                        Subtitle: 1,
                        title: 1,
                        location: 1,
                        Pincode: 1,
                        orderType: 1,
                        Price: 1,
                        Rating: 1,
                        SubRating: 1,
                        Address: 1,
                        image: 1,
                    }
                }
            ]),
            await shopModel.countDocuments()
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('shop'), {
            shop_data: shop_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(shop_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const update_shop = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        id = body?.id,
        search = new RegExp(`^${body.name}$`, "ig")
    let user: any = req.header('user')
    body.createdBy = user._id
    try {
        let isExist = await shopModel.findOne({ title: req.body.title, isActive: true })
        if (isExist) return res.status(200).json(new apiResponse(200, responseMessage?.dataAlreadyExist('shop title'), {}, {}));
        let response = await shopModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, body)

        if (body?.image && response?.image) {
            let delete_image = await not_first_one(response?.image, body?.image)
            await delete_image.map(async data => {
                let [folder_name, image_name] = await URL_decode(data)
                await deleteImage(image_name, folder_name)
            })
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('shop'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.updateDataError('shop'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

export const delete_shop = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id,
        body = req.body

    try {
        let response = await shopModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { isActive: false })
        console.log(1)
        if (body?.image && response?.image) {
            let delete_image = await not_first_one(response?.image, body?.image)
            await delete_image.map(async data => {
                let [folder_name, image_name] = await URL_decode(data)
                await deleteImage(image_name, folder_name)
            })
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.deleteDataSuccess('shop'), {}, {}));
        console.log(2)
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}


