"use strict"
import { reqInfo } from '../../helpers/winston_logger'
import { userModel } from '../../database'
import { loginStatus, apiResponse, URL_decode, SMS_message, userStatus } from '../../common'
import bcryptjs from 'bcryptjs'
import jwt from 'jsonwebtoken'
import config from 'config'
import { Request, Response } from 'express'
import { sendSMS } from '../../helpers/aws_sns'
import axios from 'axios'
import { forgot_password_mail, email_verification_mail } from '../../helpers/aws_mail'
import { deleteImage } from '../../helpers/S3'
import { responseMessage } from '../../helpers/response'

const ObjectId = require('mongoose').Types.ObjectId
const jwt_token_secret = config.get('jwt_token_secret')

export const admin_signUp = async (req: Request, res: Response) => {
    reqInfo(req)

    try {
        let body = req.body,
            otpFlag = 1, // OTP has already assign or not for cross-verification
            authToken = 0
        //  if (body.userType != 1) return res.status(409).json(new apiResponse(409, responseMessage.onlyadminRegister, {}, {}));
        let isAlready: any = await userModel.findOne({ email: body?.email, isActive: true, userType: userStatus.admin })
        if (isAlready?.email == body?.email) return res.status(409).json(new apiResponse(409, responseMessage?.alreadyEmail, {}, {}))
        if (isAlready?.isBlock == true) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}))
        while (otpFlag == 1) {
            for (let flag = 0; flag < 1;) {
                authToken = await Math.round(Math.random() * 1000000)
                if (authToken.toString().length == 6) {
                    flag++
                }
            }
            let isAlreadyAssign = await userModel.findOne({ otp: authToken })
            if (isAlreadyAssign?.otp != authToken) otpFlag = 0
        }
        body.authToken = authToken
        body.otp = authToken
        body.otpExpireTime = new Date(new Date().setMinutes(new Date().getMinutes() + 10))
        await new userModel(body).save().then(async data => {
            await sendSMS(data?.countryCode, data?.phoneNumber, `${SMS_message.OTP_verification} ${data?.otp}`)
        })
        return res.status(200).json(new apiResponse(200, responseMessage?.signupSuccess, {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const admin_login = async (req: Request, res: Response) => {
    let body = req.body;
    reqInfo(req)
    try {
        // let response = await userModel.findOneAndUpdate({ email: body.email, isActive: true, userType: 1 }, { $addToSet: { deviceToken: body?.deviceToken } }).select('-__v -createdAt -updatedAt')
        let response = await userModel.findOne({ email: body.email, isActive: true, userType: 1 }).select('-__v -createdAt -updatedAt')
        if (!response) return res.status(400).json(new apiResponse(400, responseMessage?.invalidUserPasswordEmail, {}, {}));
        if (response.isEmailVerified == false) return res.status(502).json(new apiResponse(502, responseMessage?.emailUnverified, {}, {}));
        if (response?.isUserActive == false) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}));
        const passwordMatch = new bcryptjs.compare(body.password, response.password)
        if (!passwordMatch) return res.status(400).json(new apiResponse(400, responseMessage?.invalidUserPasswordEmail, {}, {}));
        const token = jwt.sign({
            _id: response._id,
            authToken: response.authToken,
            type: response.userType,
            status: "Login",
            generatedOn: (new Date().getTime())
        }, jwt_token_secret)
        response = {
            phoneNumber: response.phoneNumber,
            firstName: response.firstName,
            lastName: response.lastName,
            email: response.email,
            image: response.image,
            token,
        }
        return res.status(200).json(new apiResponse(200, responseMessage?.loginSuccess, response, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage.internalServerError, {}, error));
    }
}

export const forgot_password = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        otpFlag = 1, // OTP has already assign or not for cross-verification
        otp = 0
    try {
        body.isActive = true
        let data = await userModel.findOne(body)
        if (!data) return res.status(400).json(new apiResponse(400, responseMessage?.invalidEmail, {}, {}));
        if (data.isUserActive == false) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}));
        if (data?.isEmailVerified == false) return res.status(502).json(new apiResponse(502, responseMessage?.emailUnverified, {}, {}));

        while (otpFlag == 1) {
            for (let flag = 0; flag < 1;) {
                otp = await Math.round(Math.random() * 1000000)
                if (otp.toString().length == 6) {
                    flag++
                }
            }
            let isAlreadyAssign = userModel.findOne({ otp: otp })
            if (isAlreadyAssign?.otp != otp) otpFlag = 0
        }
        let response = await forgot_password_mail(data, otp)
        if (response) {
            await userModel.findOneAndUpdate(body, { otp, otpExpireTime: new Date(new Date().setMinutes(new Date().getMinutes() + 10)) })
            return res.status(200).json(new apiResponse(200, `${response}`, {}, {}));
        }
        else return res.status(501).json(new apiResponse(501, responseMessage?.errorMail, {}, `${response}`));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage.internalServerError, {}, error));
    }
}
export const admin_otp_verification = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    try {
        body.isActive = true
        let data = await userModel.findOneAndUpdate(body, { otp: null, otpExpireTime: null, isEmailVerified: true, authToken: body.otp })

        if (!data) return res.status(400).json(new apiResponse(400, responseMessage?.invalidOTP, {}, {}));
        if (data.isUserActive == false) return res.status(403).json(new apiResponse(403, responseMessage?.accountBlock, {}, {}));
        if (new Date(data.otpExpireTime).getTime() < new Date().getTime()) return res.status(410).json(new apiResponse(410, responseMessage?.expireOTP, {}, {}));

        if (data.isEmailVerified == false) {
            return res.status(200).json(new apiResponse(200, responseMessage?.EmailVerificationComplete, { action: "Please go to login page" }, {}));
        }
        else {
            if (data) return res.status(200).json(new apiResponse(200, responseMessage?.OTPverified, { _id: data._id }, {}));
            else return res.status(501).json(new apiResponse(501, responseMessage?.errorMail, {}, data));
        }
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage.internalServerError, {}, error));
    }
}
export const update_profile = async (req: Request, res: Response) => {
    reqInfo(req)
    let user: any = req.header('user'), emailAlreadyExist: any,
        id = req.body?.id,
        body: any = req.body
    try {
        // console.log(body?.email)
        // if (body?.email) {
        //     emailAlreadyExist = await userModel.findOne({ email: body?.email, _id: { $ne: ObjectId(id) }, isActive: true }, { email: 1 })
        //     if (emailAlreadyExist) return res.status(409).json(new apiResponse(404, responseMessage?.alreadyEmail, {}, {}))
        // }
        let response = await userModel.findOneAndUpdate({ _id: ObjectId(user._id), isActive: true }, body)
        if (body?.profile_image != response?.profile_image && response.profile_image != null && body?.profile_image != null && body?.profile_image != undefined) {
            let [folder_name, image_name] = await URL_decode(response?.profile_image)
            await deleteImage(image_name, folder_name)
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('Your profile'), {}, {}))
        else return res.status(501).json(new apiResponse(501, responseMessage?.updateDataError('Your profile'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, {}))
    }
}
export const reset_password = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        authToken = 0,
        id = body.id
    try {
        const salt = await bcryptjs.genSaltSync(10)
        const hashPassword = await bcryptjs.hash(body.password, salt)
        delete body.password
        delete body.id
        body.password = hashPassword

        for (let flag = 0; flag < 1;) {
            authToken = await Math.round(Math.random() * 1000000)
            if (authToken.toString().length == 6) {
                flag++
            }
        }
        body.authToken = authToken
        let response = await userModel.findOneAndUpdate({ _id: id, isActive: true }, body)
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.resetPasswordSuccess, { action: "Please go to login page" }, {}));
        else return res.status(501).json(new apiResponse(501, responseMessage?.resetPasswordError, {}, response));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage.internalServerError, {}, error));
    }
}
export const get_profile = async (req: Request, res: Response) => {
    reqInfo(req)
    let user: any = req.header('user')
    try {
        let response = await userModel.findOne({ _id: ObjectId(user._id), isActive: true, userType: userStatus?.admin })
        if (response) return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('Your profile'), response, {}))
        else return res.status(501).json(new apiResponse(501, responseMessage.getDataNotFound('profile'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, {}))
    }
}
