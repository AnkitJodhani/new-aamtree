import { reqInfo } from '../../helpers/winston_logger'
import { productModel, shopModel } from '../../database'
import { apiResponse, not_first_one, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import async from 'async'
import { deleteImage } from '../../helpers/S3'
import { responseMessage } from '../../helpers/response'
const ObjectId = mongoose.Types.ObjectId

export const add_product = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    let user: any = req.header('user')
    try {
        let isExist = await productModel.findOne({ title: req.body.title, isActive: true })
        if (isExist) return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('product'), {}, {}));
        req.body.createdBy = (req.header('user') as any)?._id
        let response = await new productModel(req.body).save()
        if (response) return res.status(409).json(new apiResponse(409, responseMessage?.addDataSuccess('product'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));


    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, "Internal Server Error", {}, error))
    }
}
export const update_product = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body,
        id = body?.id,
        user: any = req.header('user')
    body.createdBy = user._id
    try {
        let isExist = await productModel.findOne({ title: body?.title, isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('product'), {}, {}));
        }
        let response = await productModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, body)
        if (body?.image && response?.image) {
            let delete_image = await not_first_one(response?.image, body?.image)
            await delete_image.map(async data => {
                let [folder_name, image_name] = await URL_decode(data)
                await deleteImage(image_name, folder_name)
            })
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('product'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.updateDataError('product'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const delete_product = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id,
        body = req.body

    try {
        let response = await productModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { isActive: false })
        if (body?.image && response?.image) {
            let delete_image = await not_first_one(response?.image, body?.image)
            await delete_image.map(async data => {
                let [folder_name, image_name] = await URL_decode(data)
                await deleteImage(image_name, folder_name)
            })
        }
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.deleteDataSuccess('product'), {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_product_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit } = req.body, match: any = {}, skip = 0
    try {
        match.isActive = true
        let [product_data, product_count] = await async.parallel([
            (callback) => {
                productModel.aggregate([
                    { $match: match },
                    { $sort: { createdAt: -1 } },
                    { $skip: skip },
                    { $limit: limit },
                ]).then(data => { callback(null, data) }).catch(err => { console.log(err) })
            },
            (callback) => { productModel.countDocuments({ isActive: true }).then(data => { callback(null, data) }).catch(err => { console.log(err) }) },
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('product'), {
            product_data: product_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(product_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const by_id_product = async (req: Request, res: Response) => {
    reqInfo(req)
    let { id } = req.params
    try {
        let response = await productModel.findOne({ _id: ObjectId(id), isActive: true })
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('product'), response, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
























