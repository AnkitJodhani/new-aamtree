"use strict"
import { reqInfo } from '../../helpers/winston_logger'
import { categoryModel } from '../../database'
import { apiResponse } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import async from 'async'
import { responseMessage } from '../../helpers/response'

const ObjectId = mongoose.Types.ObjectId

export const get_category_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit, search } = req.body, match: any = {}
    try {
        if (search) {
            var nameArray: Array<any> = []
            search = search.split(" ")
            search.forEach(data => {
                nameArray.push({ "name": { $regex: data, $options: 'si' } })
            })
            match['$or'] = [{ $and: nameArray }]
        }
        match.isActive = true

        let [category_data, category_count] = await async.parallel([
            (callback) => {
                categoryModel.aggregate([
                    { $match: { isActive: true } },
                    { $match: match },
                    { $sort: { createdAt: -1 } },
                    { $skip: ((((page) as number - 1) * (limit) as number)) },
                    { $limit: limit },
                ]).then(data => { callback(null, data) }).catch(err => { console.log(err) })
            },
            (callback) => { categoryModel.countDocuments({ isActive: true }).then(data => { callback(null, data) }).catch(err => { console.log(err) }) },
        ])
        return res.status(200).json(new apiResponse(200, responseMessage.getDataSuccess('category'), {
            software_data: category_data,
            state: {
                page: page,
                limit: limit,
                page_limit: Math.ceil(category_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const by_id_category = async (req: Request, res: Response) => {
    reqInfo(req)
    let { id } = req.params
    try {
        let response = await categoryModel.findById(id)
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('category'), response, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, "Internal Server Error", {}, error))
    }
}
