"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { addressModel } from '../../database'
import { apiResponse, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'
const ObjectId = mongoose.Types.ObjectId
export const add_address = async (req: Request, res: Response) => {
    reqInfo(req)
    req.body.createdBy = (req.header('user') as any)?._id
    try {
        let response = await new addressModel(req.body).save()
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('address'), {}, {}));
        else return res.status(400).json(new apiResponse(400, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const update_address = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.body?.id,
        body: any = req.body
    delete body?.id
    body.updatedBy = (req.header('user') as any)?._id
    try {
        let response = await addressModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, body, { new: true }).select({ address: 1, state: 1, isActive: 1, PINcode: 1, city: 1, location: 1 })
        if (response) {
            return res.status(200).json(new apiResponse(200, responseMessage?.updateDataSuccess('address'), {}, {}));
        }
        else return res.status(404).json(new apiResponse(404, responseMessage?.updateDataError(''), {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_address = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.body?.id
    try {
        let response = await addressModel.find({ isActive: true })
        console.log(response)
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('address'), response, {}));
        else return res.status(404).json(new apiResponse(404, responseMessage?.getDataNotFound(''), {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const by_id_address = async (req: Request, res: Response) => {
    reqInfo(req)
    let { id } = req.params
    try {
        let response = await addressModel.findOne({ _id: ObjectId(id), isActive: true })
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('address'), response, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const delete_address = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let response = await addressModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { isActive: false })
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.deleteDataSuccess('address'), {}, {}));

    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

