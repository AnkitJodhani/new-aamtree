"use strict"
import { logger, reqInfo } from '../../helpers/winston_logger'
import { cartModel } from '../../database'
import { apiResponse, URL_decode } from '../../common'
import { Request, Response } from 'express'
import mongoose from 'mongoose'
import { deleteImage } from '../../helpers/S3'
import async from 'async'
import { responseMessage } from '../../helpers/response'
import { Object } from 'aws-sdk/clients/appflow'

const ObjectId = mongoose.Types.ObjectId

export const add_cart = async (req: Request, res: Response) => {
    reqInfo(req)
    let body = req.body
    let user: any = req.header('user')
    body.createdBy = user._id
    try {
        let isExist = await cartModel.findOne({ productId: ObjectId(body?.projectId), isActive: true })
        if (isExist) {
            return res.status(409).json(new apiResponse(409, responseMessage?.dataAlreadyExist('cart with product'), {}, {}));
        }
        let response = await new cartModel(body).save()
        if (response) return res.status(200).json(new apiResponse(200, responseMessage?.addDataSuccess('cart'), {}, {}));
        else return res.status(409).json(new apiResponse(409, responseMessage?.addDataError, {}, {}));
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

export const increment_cart_quantity = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let cartData = await cartModel.aggregate([
            { $match: { _id: ObjectId(id), isActive: true } },
            {
                $lookup: {
                    from: "products",
                    let: { productId: '$productId' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$productId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            }
                        },
                        {
                            $project: { price: 1, }
                        }
                    ],
                    as: "product"
                }
            },
        ])
        console.log(cartData?.length == 0);
        console.log(cartData);


        if (cartData?.length == 0) {
            return res.status(404).json(new apiResponse(404, responseMessage?.getDataNotFound('cart'), {}, {}));
        }
        await cartModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { $inc: { quantity: 1, price: cartData[0]?.product[0]?.price } })
        return res.status(200).json(new apiResponse(200, responseMessage?.customMessage('Cart successfully incremented!'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}

export const decrement_cart_quantity = async (req: Request, res: Response) => {
    reqInfo(req)
    let id = req.params.id
    try {
        let cartData = await cartModel.aggregate([
            { $match: { _id: ObjectId(id), isActive: true } },
            {
                $lookup: {
                    from: "products",
                    let: { productId: '$productId' },
                    pipeline: [
                        {
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$productId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            }
                        },
                        {
                            $project: { price: 1, }
                        }
                    ],
                    as: "product"
                }
            },
        ])
        if (cartData?.length == 0) {
            return res.status(404).json(new apiResponse(404, responseMessage?.getDataNotFound('cart'), {}, {}));
        }
        if (cartData[0]?.quantity == 0)
            return res.status(400).json(new apiResponse(400, responseMessage?.customMessage('Cart quantity already 0'), {}, {}))
        await cartModel.findOneAndUpdate({ _id: ObjectId(id), isActive: true }, { $inc: { quantity: -1, price: -cartData[0]?.product[0]?.price } })
        return res.status(200).json(new apiResponse(200, responseMessage?.customMessage('Cart successfully decremented!'), {}, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
export const get_cart_pagination = async (req: Request, res: Response) => {
    reqInfo(req)
    let { page, limit, page_limit } = req.body, match: any = {}, user: any = req.header('user'), response: any, skip: any = {}, cart_count: any = {}
    try {


        [response, cart_count] = await Promise.all([
            await cartModel.aggregate([
                { $match: match },
                {
                    $lookup: {
                        from: "carts",
                        let: { cartId: '$cartId', },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$cartId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            },
                        }, {
                            $project: { productId: 1, quantity: 1, price: 1, cartTotal: 1, delivery: 1, discount: 1, orderTotal: 1 }
                        }
                        ],
                        as: "carts"
                    },
                },
            ]),
            await cartModel.aggregate([
                { $match: match },
                {
                    $lookup: {
                        from: "carts",
                        let: { cartId: '$cartId', },
                        pipeline: [{
                            $match: {
                                $expr: {
                                    $and: [
                                        { $eq: ['$_id', '$$cartId'] },
                                        { $eq: ['$isActive', true] },
                                    ],
                                },
                            },
                        }, {
                            $project: { productId: 1, quantity: 1, price: 1, cartTotal: 1, delivery: 1, discount: 1, orderTotal: 1 }
                        }
                        ],
                        as: "carts"
                    },
                },
                { $sort: { createdAt: -1 } },
                { $limit: limit },
            ]),
            await cartModel.countDocuments(match)
        ])
        return res.status(200).json(new apiResponse(200, responseMessage?.getDataSuccess('cart'), {
            cart_data: response,
            state: {
                page,
                limit,
                page_limit: Math.ceil(cart_count / (limit) as number)
            }
        }, {}))
    } catch (error) {
        console.log(error)
        return res.status(500).json(new apiResponse(500, responseMessage?.internalServerError, {}, error))
    }
}
