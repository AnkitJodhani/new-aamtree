import fs from 'fs'
import { upload_all_type } from './S3'
import pdf from 'pdf-creator-node'
import { invoice_html } from './invoice'

export const invoice_pdf_generation = async (body_data: any, upload_location: any) => {
    return new Promise(async function (resolve, reject) {
        try {
            const html = await invoice_html(body_data)
            const filename = body_data?.invoice_no + '.pdf'
            const document = {
                html: html,
                data: {
                    details: [body_data]
                },
                path: './' + filename
            }
            await pdf.create(document, {})
                .then(res => {
                    return res
                }).catch(error => {
                    console.log(error)
                    return reject(error)
                })
            let location = await upload_all_type({
                data: fs.readFileSync(process.cwd() + `/${filename}`),
                name: filename,
                mimetype: 'application/pdf'
            }, upload_location)
            fs.unlinkSync(process.cwd() + `/${filename}`)
            return resolve(location)
        }
        catch (error) {
            reject(error)
        }
    })
}
