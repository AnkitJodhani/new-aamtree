import { inWords } from "../common"

export const invoice_html = (data) => {
    let wordText = inWords(parseInt(data?.finalAmount)), tax_invoice = '', service_invoice = ''
    if (data?.sameState) {
        tax_invoice += `<table style="width: 100%; border-collapse: collapse;">
    <tr>
      <td style="padding: 10px 0; width: 50.5%;">
        <table style="width: 100%; border-collapse: collapse;">
          <tr>
            <td><span style="display: inline-block; width: 50%;">CGST</span>
              <span style="display: inline-block; width: 35%; text-align: right;">9%</span>
            </td>
          </tr>
          <tr>
            <td><span style="display: inline-block; width: 50%;">SGST</span>
              <span style="display: inline-block; width: 35%; text-align: right;">9%</span>
            </td>
          </tr>
          <tr>
            <td>TCS</td>
          </tr>
        </table>
      </td>
      <td style="text-align: right; padding: 10px 0; width: 110px; border-left: 1px solid #000;">
        <table style="width: 100%; border-collapse: collapse;">
          <tr>
            <td>${data?.CGST}</td>
          </tr>
          <tr>
            <td>${data?.SGST}</td>
          </tr>
          <tr>
            <td>00.00</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>`
    }
    else {
        tax_invoice += ` <table style="width: 100%; border-collapse: collapse;">
    <tr>
      <td style="padding: 10px 0;width: 50%;">
        <table style="width: 100%; border-collapse: collapse;">
          <tr>
            <td><span style="display: inline-block; width: 50%;">IGST</span>
              <span style="display: inline-block; width: 35%; text-align: right;">18%</span>
            </td>
          </tr>
          <tr>
            <td>TCS</td>
          </tr>
        </table>
      </td>
      <td style="text-align: right; padding: 10px 0; width: 110px; border-left: 1px solid #000;">
        <table style="width: 100%; border-collapse: collapse;">
          <tr>
            <td>${data?.IGST}</td>
          </tr>
          <tr>
            <td>00.00</td>
          </tr>
        </table>
      </td>
    </tr>
  </table>`
    }
    if (data?.cost_type == 0) {
        service_invoice = `<p style="margin:0;">${data?.categoryName}-${data?.subCategoryName}-${data?.high_light} (HSN Code 998531)</p>
    <span>-</span>
    <p style="margin:0;"><span>${data?.createdAt}</span> <span>${data?.dateTime}</span></p>`
    }
    if (data?.cost_type == 1) {
        service_invoice = `<p style="margin:0;">${data?.categoryName}-${data?.subCategoryName}-${data?.high_light} (HSN Code 998531)</p>
    <span>Qty ${data?.input_cost[0]} Rate ${Object.values(data?.cost[0])[0]}</span>
    <p style="margin:0;"><span>${data?.createdAt}</span> <span>${data?.dateTime}</span></p>`
    }
    if (data?.cost_type == 2) {
        let str = ""
        data?.input_cost.forEach(data => {
            // <span> Single seater sofa Qty 2 Rate 500 </span>`
            str += `<span>${Object.keys(data)} Qty ${(Object.values(data as any)[0] as any)?.quantity} Rate ${(Object.values(data as any)[0] as any)?.unitPrice}</span><br>`
        })
        service_invoice = `<p style="margin:0;">${data?.categoryName}-${data?.subCategoryName}-${data?.high_light} (HSN Code 998531)</p>
    ${str}
    <p style="margin:0;"><span>${data?.createdAt}</span> <span>${data?.dateTime}</span></p>`
    }
    let invoice = `<!DOCTYPE html>
    <html lang="en">
    
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Invoice</title>
    </head>
  
    <body style="margin:0;">
      <table
          style="font-family: arial; max-width: 550px; width: 100%; height: 100%; margin-left: auto; margin-right: auto; font-size: 0.55rem; border: 1px solid #000; border-collapse: collapse;">
  
          <tr>
              <td style="padding:5px 10px 0;">
                  <table>
                      <tr>
                          <td>
                              <img src="https://vpristine.s3.ap-south-1.amazonaws.com/PVR.png" alt="PVR"
                                  style="width:70px;">
                          </td>
                          <td style="text-align: center; width: 90%;">
                              <p style="margin: 0 0 3px 0; font-weight: bold;">TAX INVOICE</p>
                              <p style="margin: 0 50px 3px 50px;"><span style="font-weight: bold;">PVR LIMITED</span>,
                                  ${data?.storeAddress}</p>
                              <div style="margin:0 50px; ">
                                  <p style="display: inline-block; width: 49%; text-align: left;"><span
                                          style="font-weight: bold;">GSTIN
                                          NO : </span>${data?.storeGST}</p>
                                  <p style="display: inline-block; width: 49%; text-align: right;"><span
                                          style="font-weight: bold;">PAN NO
                                          : </span>AAACP4526D</p>
                              </div>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
  
          <tr>
              <td style="padding:0;">
                  <p style="padding: 5px 10px; border-top: 1px solid #000; border-bottom: 1px solid #000; margin-top: 0;">
                      Booking No : ${data?.booking_no}</p>
                  </p>
                  <table style="width: 100%; border-collapse: collapse;">
                      <tr>
                          <td style="padding:0 10px; width: 50%;">
                              <table style="width: 100%; border-collapse: collapse;">
                                  <tr>
                                      <td>
                                          <p style="margin:0 0 5px 0;">PVR Ref. No. : N/A</p>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <p style="margin:0 0 5px 0;">Cust Ref No. : N/A</p>
                                      </td>
                                  </tr>
  
                              </table>
                          </td>
                          <td style="padding:0 10px; width: 50%;">
                              <table style="width: 100%; border-collapse: collapse;">
                                  <tr>
                                      <td>
                                          <p style="margin:0 0 5px 0;">INVOICE NO. : ${data?.invoice_no}</p>
                                      </td>
                                  </tr>
                                  <tr>
                                      <td>
                                          <p style="margin:0 0 5px 0;">INVOICE DATE. :${data?.invoice_date}</p>
                                      </td>
                                  </tr>
  
                              </table>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
  
  
          <tr>
              <td style="padding:0;">
                  <table style="width: 100%; border-collapse: collapse;">
                      <tr>
                          <td
                              style="vertical-align: top; width: 50%; padding:0; border-right: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000;">
                              <p
                                  style="border-bottom: 1px solid #000; margin: 0; padding: 5px 0; text-align: center; font-weight: bold;">
                                  Bill To</p>
  
                              <table style="width: 100%; padding: 0 10px;">
                                  <tr>
                                      <td style="padding: 2px 0;width: 60px;">NAME</td>
                                      <td style="padding: 2px 0;">${data?.userName}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;width: 60px;">Mobile No.</td>
                                      <td style="padding: 2px 0;">${data?.phoneNumber}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;">Address</td>
                                      <td style="padding: 2px 0;">${data?.userAddress}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;">State Code</td>
                                      <td style="padding: 2px 0;">${data?.stateCode}</td>
                                  </tr>
                              </table>
                          </td>
  
                          <td
                              style="vertical-align: top; width: 50%; padding:0; border-top: 1px solid #000; border-bottom: 1px solid #000;">
                              <p
                                  style="border-bottom: 1px solid #000; margin: 0; padding: 5px 0; text-align: center; font-weight: bold;">
                                  Ship To</p>
                              <table style="width: 100%; padding: 0 10px;">
                                  <tr>
                                      <td style="padding: 2px 0;width: 60px;">NAME</td>
                                      <td style="padding: 2px 0;">${data?.userName}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;width: 60px;">Mobile No.</td>
                                      <td style="padding: 2px 0;">${data?.phoneNumber}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;">Address</td>
                                      <td style="padding: 2px 0;">${data?.userAddress}</td>
                                  </tr>
                                  <tr>
                                      <td style="padding: 2px 0;">State Code</td>
                                      <td style="padding: 2px 0;">${data?.stateCode}</td>
                                  </tr>
                              </table>
                          </td>
                      </tr>
                  </table>
              </td>
          </tr>
  
          <tr>
              <td style="padding:0;">
                  <table style="width: 100%; border-collapse: collapse;">
                      <thead>
                          <tr style="border-bottom: 1px solid #000;">
                              <th style="padding: 5px 0; width: 10%; border-right: 1px solid #000;">Sr. No</th>
                              <th style="padding: 5px 0; width: 70%;">Particulars</th>
                              <th
                                  style="width: 100px; width: 20%; text-align: right; padding: 5px 10px; border-left: 1px solid #000;">
                                  Taxable Value</th>
                          </tr>
                      </thead>
                      <tbody style="vertical-align: top;">
  
                          <tr>
                              <td style="padding: 10px; text-align: center; border-right: 1px solid #000;">1</td>
                              <td style="padding: 10px;">
                                  ${service_invoice}
                              </td>
                              <td style="text-align: right; padding: 10px; border-left: 1px solid #000;">${data?.amount}
                              </td>
                          </tr>
  
                      </tbody>
                      <tfoot>
                          <tr style="border-top: 1px solid; border-bottom: 1px solid;">
                              <td style="padding: 0px" colspan="3">
                                  <table style="width: 100%; border-collapse: collapse;">
                                      <tr>
                                          <td style="padding:0 10px; width: 60%;">
                                              <p style="margin:0 0 7px 0;">Note : Please do not deduct the TDS on GST
                                                  Value of the Invoice</p>
                                              <div>
                                                  <p style="margin: 0 20px 0 0; display: inline-block;"><span>PAYMENT
                                                          TERMS : </span>ADVANCE</p>
                                                  <p style="margin: 0; display: inline-block;"><span>REVERSE CHARGE :
                                                      </span>No</p>
                                              </div>
                                          </td>
                                          <td style="padding:0 10px; width: 40%;">
                                              ${tax_invoice}
  
                                          </td>
                                      </tr>
                                      <tr style="border-top: 1px solid #000;">
                                          <td></td>
                                          <td style="padding:0 10px; width: 30%;">
                                              <table style="width: 100%; border-collapse: collapse;">
                                                  <tr>
                                                      <td style="padding: 10px;width: 50.5%;">
                                                          <table style="width: 100%; border-collapse: collapse;">
                                                          <tr>
                                                          <td style="text-align: right;  font-weight: bold;">TOTAL
                                                          </td>
                                                          </tr>
                                                          <tr>
                                                          <td style="text-align: right;  font-weight: bold;">DISCOUNT(${data?.discountPercentage}%)
                                                          </td></tr>
                                                          <tr><td style="text-align: right;  font-weight: bold;">GRAND TOTAL
                                                          </td>
                                                        </tr>
                                                          </table>
                                                      </td>
                                                      <td
                                                          style="text-align: right; padding: 10px 0; width: 110px; border-left: 1px solid #000;">
                                                          <table style="width: 100%; border-collapse: collapse;">
                                                              <tr>
                                                                  <td style="font-weight: bold;">${parseFloat(data?.finalAmount as string) + parseFloat(data.discount as string)}</td>
                                                              </tr>
                                                              <tr>
                                                                  <td style="font-weight: bold;">- ${data.discount}</td>
                                                              </tr>
                                                              <tr>
                                                                  <td style="font-weight: bold;">${data?.finalAmount}</td>
                                                              </tr>
                                                          </table>
                                                      </td>
                                                  </tr>
                                              </table>
                                          </td>
                                      </tr>
                                  </table>
                              </td>
                          </tr>
                      </tfoot>
                  </table>
              </td>
          </tr>
  
          <tr>
              <td style="padding:10px">
                  <div>
                      <p style="margin: 0 0 5px 0; display: inline-block; width: 49.5%; text-align: left;">Amount
                          Chargable (In Words)</p>
                      <p style="margin: 0 0 5px 0; display: inline-block; width: 45%; text-align: right;">E. & O.E.</p>
                  </div>
                  <p style="margin-bottom: 3px; font-weight: bold; margin-top: 5px;">${wordText}</p>
                  <div>
                      <p style="margin: 0 0 5px 0;">1) Any Discrepancy in this bill should be notified within 5 days of
                          receipt, else acceptance shall be deemed</p>
                      <p style="margin: 0 0 5px 0;">2) For any query regarding this bill please mail at
                          feedback@vpristine.com</p>
                      <p style="margin: 0 0 5px 0;">3) All Disputes subject to Delhi Jurisdiction only.</p>
                  </div>
              </td>
          </tr>
  
          <tr>
              <td style="padding: 0 10px;">
                  <p style="text-align: center; font-weight: bold;">Corporate Office : PVR LIMITED, Block A, 4th Floor,
                      Building No.9, DLF Cyber City Phase-III, Gurgaon -122 002 Tel : 0124-4708100, Registered Office :
                      61, Basant Lok, Vasant Vihar, New Delhi-110 057 (CIN No. : L74899DL1995PLC067827)</p>
              </td>
          </tr>
  
      </table>
  </body>
  
  </html>`
    return invoice
}