
import { Request, Router, Response } from 'express'
import { uploadRouter } from './upload'
import { userRouter } from './user'
import { adminRouter } from './admin'
import { userStatus } from '../common'
const router = Router()

const accessControl = (req: Request, res: Response, next: any) => {
    req.headers.userType = userStatus[req.originalUrl.split('/')[1]]
    next()
}

router.use('/user', userRouter)
router.use('/upload', uploadRouter)
router.use('/admin', adminRouter)

export default router
