var mongoose = require('mongoose')
const cartSchema = new mongoose.Schema({
    productId: { type: mongoose.Schema.Types.ObjectId },
    quantity: { type: Number, default: 0 },
    price: { type: Number, default: 0 },
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    cartTotal: { type: Number, default: 0 },
    delivery: { type: Number, default: 0 },
    discount: { type: Number, default: 0 },
    orderTotal: { type: Number, default: 0 },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const cartModel = mongoose.model('cart', cartSchema) 