var mongoose = require('mongoose')
const shopSchema = new mongoose.Schema({
    title: { type: String, default: null },
    Subtitle: { type: String, default: null },
    isActive: { type: Boolean, default: true, },
    orderType: { type: Number, default: 0, enum: [0, 1] },// 0 - Delivery || 1 - pickup  
    location: { type: { type: String, default: "Point" }, coordinates: { type: Array, default: [] } },
    isBlock: { type: Boolean, default: false, },
    Price: { type: Number, default: 0 },
    Rating: { type: Number, default: 0 },
    SubRating: { type: Number, default: 0 },
    Address: { type: String, default: null },
    State: { type: String, default: null },
    City: { type: String, default: null },
    Pincode: { type: String, default: null },
    OpeningTime: { type: String, default: Date.now() },
    EndingTime: { type: String, default: Date.now() },
    image: { type: [{ type: String }], default: [] },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const shopModel = mongoose.model('shop', shopSchema) 