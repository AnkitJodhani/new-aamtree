var mongoose = require('mongoose')
const trackOrderSchema = new mongoose.Schema({

    estimated_Date: { type: Date, default: 0 },
    pickup_Date: { type: Date, default: 0 },
    order: { type: Boolean, default: true },
    shipped: { type: Boolean, default: true },
    dispatched: { type: Boolean, default: true },
    arriving: { type: Boolean, default: true },
    isActive: { type: Boolean, default: true, },
    isBlock: { type: Boolean, default: false, },
    createdBy: { type: mongoose.Schema.Types.ObjectId },
    updatedBy: { type: mongoose.Schema.Types.ObjectId },
}, { timestamps: true })

export const trackOrderModel = mongoose.model('track_order', trackOrderSchema)