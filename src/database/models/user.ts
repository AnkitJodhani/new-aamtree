const mongoose = require('mongoose')

const userSchema: any = new mongoose.Schema({
    firstName: { type: String, default: null },
    lastName: { type: String, default: null },
    countryCode: { type: Number, default: 91 },
    phoneNumber: { type: String, default: null },
    email: { type: String, default: null },
    password: { type: String, default: null },
    profile_image: { type: String, default: null },
    image: { type: Array, default: [] },
    experience: { type: String, default: null },
    skillId: { type: mongoose.Schema.Types.ObjectId },
    categoryId: { type: mongoose.Schema.Types.ObjectId },
    rate: { type: Number, default: 0 },
    authToken: { type: Number, default: null },
    otp: { type: Number, default: null },
    otpExpireTime: { type: Date, default: null },
    deviceToken: { type: [{ type: String }], default: [] },
    address: { type: String, default: null },
    city: { type: String, default: null },
    state: { type: String, default: null },
    latitude: { type: Number, default: null },
    longitude: { type: Number, default: null },
    feedback_rating: { type: Number, default: 0 },
    total_feedback: { type: Number, default: 0, min: 0 },
    loginType: { type: Number, default: 0, enum: [0, 1] }, // 0 - custom || 1 - google
    userType: { type: Number, default: 0, enum: [0, 1] }, // 0 - user || 1 - admin
    isActive: { type: Boolean, default: true },
    isBlock: { type: Boolean, default: false },
}, { timestamps: true }
)

export const userModel = mongoose.model('user', userSchema);

