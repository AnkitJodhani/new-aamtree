FROM node:14.18.2-alpine
WORKDIR /usr/src/app

COPY package*.json ./
RUN npm install 
COPY . . 
RUN npm run build

EXPOSE 80
CMD ["npm", "start"]

